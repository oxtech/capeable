# capeable
A simple dashboard to display information that makes you more _Cape_-able.

## Usage / Development
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Configuration
You'll likely need to make several changes before starting out with the app.

## API Keys
Your own API keys are required to access Google Services and Dark Sky (for weather forecasting).

#### Google
Create an app with authentication credentials at https://console.developers.google.com/apis/dashboard.

You'll need to enable access to the Photos Library and Google Calendar API for this app.

#### Dark Sky
Sign up for an API Key at https://darksky.net/dev.


## First Start
The first time you load the app you should be prompted to sign in to Google (make sure popups are allowed) and Calendar & Photos access should be requested. After being allowed, you may need to refresh the page.


## Components
All referenced configuration items are in `src/config.js` unless otherwise noted.

#### Clock
A simple clock that displays time and date and updates on a given interval (defaults to every second).

#### Forecast
Utilizes the Dark Sky API to get a local forecast for your area and look ahead for the next several days.

Config Key | Description
--- | ---
`appConfig.componentConfigs.skycons.color` | Color of the animated icons associated with weather conditions
`appConfig.user.weather.coords` | Your coordinates as a comma separated string (e.g. `43.033231,-88.096292`)
`appConfig.user.weather.apiKey` | Your Dark Sky API key. Defaults to reading `VUE_APP_DARKSKY_APIKEY` from the process environment.
`appConfig.user.weather.daysAhead` | How many days of additional forecast should be added
`appConfig.user.weather.precipThreshold` | For the look-ahead forecast, only display the chance of precipitation if it crosses this threshold (e.g. *.15* is a *15% chance*)
`appConfig.user.weather.duration` | How often the component should update, in seconds

#### GCalendar
*Coming Soon*

#### GPhotoFrame
Displays your last number of Google Photos, cycling through them on a defined schedule.

**Note** - It's generally advised to keep your `maxHeight` and `maxWidth` numbers the same to tolerate aspect ratio differences the best, but the defaults are framed around using a 1080x1920 monitor for display.

Config Key | Description
--- | ---
`appConfig.user.photos.maxHeight` | Photos will be cropped to this height from Google Photos
`appConfig.user.photos.maxWidth` | Photos will be cropped to this width from Google Photos
`appconfig.user.photos.duration` | How often the photo library should be updated

#### GUpcoming
Displays upcoming events from Google Calendar.

Config Key | Description
--- | ---
`appConfig.user.calendar.calendarId` | Most likely your email address - the ID of the calendar to display
`appConfig.user.calendar.maxEvents` | The number of events to display in the list
`appconfig.user.calendar.duration` | How often the app should sync with Google to update events

#### Motd
**M**essage **O**f **T**he **D**ay, renders arbitrary Markdown from an external file and displays it.

Config Key | Description
--- | ---
`appConfig.user.motd.path` | The path to the external Markdown file to render, defaults to `./assets/motd.md`.


<sub>Initially built by Justin with inspriation from the fine folks at [Black Cape](https://blackcape.io/).<sub>