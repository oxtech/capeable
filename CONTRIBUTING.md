# Contributing to Capeable
Contributions to Capeable are welcome and we appreciate your support.

This project is meant to be a learning project / hackathon idea / Vue training course, and expecations for the quality of code and contribution should reflect that.

## Issues
Feel free to self report issues. There are so many things we could do to improve the app, so all contributions in this regard are welcome. 

**Please be sure not to include any sensitive information.**

1. What version of Node/NPM are you using?
2. What triggered the problem?
3. What did you expect?
4. What happened instead?

## Contributing Code
Pull requests are always welcome.

## Most Importantly
Have fun, and don't be a jerk. Abusive or intolerant behavior will not be allowed, welcomed, encouraged, or tolerated.