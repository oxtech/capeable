module.exports = {
  chainWebpack: config => {
    config.module
    .rule('raw')
    .test(/\.md$/)
    .use('raw-loader')
    .loader('raw-loader')
    .end()
  },
  devServer: {
    proxy: {
      '/forecast': {
        target: "https://api.darksky.net",
        secure: true
      }
    }
  }
}