import Vue from 'vue'
import App from './App.vue'
import VueGoogleApi from 'vue-google-api'
import moment from 'vue-moment'
import FunctionalCalendar from 'vue-functional-calendar'
import VueSkycons from 'vue-skycons'

import appConfig from './config'

Vue.config.productionTip = false
Vue.prototype.$appConfig = appConfig

Vue.use(VueGoogleApi, appConfig.componentConfigs.google)
Vue.use(moment)
Vue.use(FunctionalCalendar, appConfig.componentConfigs.functionalCalendar)
Vue.use(VueSkycons, appConfig.componentConfigs.skycons)

new Vue({
  render: h => h(App),
}).$mount('#app')
