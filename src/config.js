export default {
  user: {
    albums: [
      'Beach 2019'
    ],
    motd: {
      path: 'motd.md'
    },
    clock: {
      duration: 1
    },
    calendar: {
      calendarId: process.env.VUE_APP_CALENDAR_ADDRESS,   // your email address, most likely
      maxEvents: 5,
      duration: 120
    },
    photos: {
      maxHeight: 1920,
      maxWidth: 1920,
      duration: 15
    },
    weather: {
      coords: '43.033231,-88.096292',     // your coordinates
      apiKey: process.env.VUE_APP_DARKSKY_APIKEY,
      daysAhead: 4,
      precipThreshold: .15,
      duration: 60 * 30
    }
  },
  system: {
    resolution: {
      height: 1920,
      width: 1080
    },
    urls: {
      photoAlbums: 'https://photoslibrary.googleapis.com/v1/albums',
      photoAlbumContents: 'https://photoslibrary.googleapis.com/v1/mediaItems:search',
      photoDetails: 'https://photoslibrary.googleapis.com/v1/mediaItems/',
      calendarEvents: 'https://www.googleapis.com/calendar/v3/calendars/',
      darkSky: 'https://api.darksky.net/forecast'
    },
  },
  componentConfigs: {
    google: {
      apiKey: process.env.VUE_APP_GOOGLE_APIKEY,
      clientId: process.env.VUE_APP_GOOGLE_CLIENTID,
      scope: process.env.VUE_APP_GOOGLE_SCOPE,
      discoveryDocs: [
        'https://photoslibrary.googleapis.com/$discovery/rest?version=v1',
        'https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'
      ],
    },
    functionalCalendar: {
      applyStylesheet: true,
      isDark: true,
      sundayStart: true,
      hiddenElements: ['navigationArrows']
    },
    skycons: {
      color: 'white'
    }
  }
}
